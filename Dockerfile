FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

ADD ./src/ /app/
ADD requirements.txt /app/
ADD alembic_docker.ini /app/alembic.ini
ADD wait-for.sh /app/

WORKDIR /app
RUN pip install -r requirements.txt && apt update && apt install -y netcat

CMD ./wait-for.sh postgres:5432 -t 120 && alembic upgrade head && /start.sh