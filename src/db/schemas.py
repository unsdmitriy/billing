from decimal import Decimal
from typing import Optional

from pydantic import BaseModel, constr, condecimal


class WalletCreate(BaseModel):
    name: constr(min_length=5, regex=r'^[a-z0-9_]+$')


class TransactionPerform(BaseModel):
    sender: Optional[str]
    recipient: str
    sum: condecimal(gt=Decimal('0'), decimal_places=2)
