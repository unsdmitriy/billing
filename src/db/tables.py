from sqlalchemy import (
    MetaData,
    Table,
    Column,
    String,
    Numeric,
    ForeignKey,
    Integer,
    DateTime,
    func,
)

Metadata = MetaData()

Wallet = Table(
    'wallet',
    Metadata,
    Column('name', String, primary_key=True),
    Column('created_at', DateTime, server_default=func.now()),
    Column('balance', Numeric, default='0.00', nullable=False)
)

Transaction = Table(
    'transaction',
    Metadata,
    Column('id', Integer, primary_key=True),
    Column('created_at', DateTime, server_default=func.now()),
    Column('sender_wallet_name', String, ForeignKey('wallet.name'), nullable=True),
    Column('recipient_wallet_name', String, ForeignKey('wallet.name'), nullable=False),
    Column('sum', Numeric, nullable=False)
)
