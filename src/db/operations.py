from decimal import Decimal

from sqlalchemy import update
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncConnection

from . import exceptions
from . import tables
from . import schemas

__all__ = [
    'wallet_create',
    'transaction_perform'
]


async def wallet_create(engine: AsyncEngine, wallet: schemas.WalletCreate):
    wallet_data = wallet.dict()

    async with engine.begin() as conn:
        stmt = insert(
            tables.Wallet
        ).values(
            **wallet_data
        ).returning(
            *tables.Wallet.c
        ).on_conflict_do_update(
            index_elements=[tables.Wallet.c.name],
            set_={tables.Wallet.c.name: wallet.name}
        )

        cursor = await conn.execute(stmt)

    return dict(cursor.one())


async def transaction_perform(engine: AsyncEngine, transaction: schemas.TransactionPerform):
    async with engine.begin() as conn:

        if transaction.sender:
            sub_balance_result = await _wallet_sub_balance(
                conn,
                name=transaction.sender,
                sum=transaction.sum
            )

            # отправитель не найден или недостаточно средств
            if not sub_balance_result.rowcount:
                raise exceptions.TransactionError

        add_balance_result = await _wallet_add_balance(
            conn,
            name=transaction.recipient,
            sum=transaction.sum
        )

        # получатель не найден
        if not add_balance_result.rowcount:
            raise exceptions.TransactionError

        result = await _write_transaction(conn, transaction)

    return dict(result.one())


async def _wallet_add_balance(conn: AsyncConnection, name: str, sum: Decimal):
    stmt = update(
        tables.Wallet
    ).values(
        balance=tables.Wallet.c.balance + sum
    ).where(
        tables.Wallet.c.name == name
    )

    return await conn.execute(stmt)


async def _wallet_sub_balance(conn: AsyncConnection, name: str, sum: Decimal):
    stmt = update(
        tables.Wallet
    ).values(
        balance=tables.Wallet.c.balance - sum
    ).where(
        tables.Wallet.c.name == name,
        tables.Wallet.c.balance >= sum
    )

    return await conn.execute(stmt)


async def _write_transaction(conn: AsyncConnection, transaction: schemas.TransactionPerform):
    payload = {
        tables.Transaction.c.sender_wallet_name: transaction.sender,
        tables.Transaction.c.recipient_wallet_name: transaction.recipient,
        tables.Transaction.c.sum: transaction.sum
    }

    stmt = insert(
        tables.Transaction
    ).values(
        payload
    ).returning(
        tables.Transaction.c.sender_wallet_name.label('sender'),
        tables.Transaction.c.recipient_wallet_name.label('recipient'),
        tables.Transaction.c.sum,
    )

    return await conn.execute(stmt)
