import uvicorn
import os
from fastapi import FastAPI, status, HTTPException
from sqlalchemy.ext.asyncio import create_async_engine

import db
import schemas

app = FastAPI()

DB_HOST = os.environ.get('DB_HOST', 'localhost')
sql_engine = create_async_engine(
    f'postgresql+asyncpg://billing_user:billing_pass@{DB_HOST}/billing',
    isolation_level='READ COMMITTED',
    pool_size=5,
    echo=True,
)


@app.get('/ping')
async def ping():
    return {'status': 'ok'}


@app.post('/api/v1/wallet', response_model=schemas.Wallet)
async def wallet_create(wallet: db.WalletCreate):
    return await db.wallet_create(sql_engine, wallet)


@app.post('/api/v1/transaction', response_model=schemas.Transaction)
async def transaction_perform(transaction: db.TransactionPerform):
    try:
        return await db.transaction_perform(sql_engine, transaction)
    except db.TransactionError:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
