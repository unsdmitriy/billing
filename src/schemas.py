from decimal import Decimal
from typing import Optional

from pydantic import BaseModel


class Wallet(BaseModel):
    name: str
    balance: Decimal


class Transaction(BaseModel):
    sender: Optional[str]
    recipient: str
    sum: Decimal
